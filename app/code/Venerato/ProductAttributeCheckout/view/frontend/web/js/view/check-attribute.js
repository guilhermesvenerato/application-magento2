define(
    [
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/quote'
    ],
    function (
        ko,
        Component,
        _,
        stepNavigator,
        quote
    ) {
        'use strict';
		
		function showStep() {
            if (((quote.getItems()[0]['product']['prop65'])!=null) && ((quote.getItems()[0]['product']['prop65']) == 1)) 
                return true;
            else 
                return false;       
        }
		
        /**
        * check-attribute - is the name of the component's .html template
        */
        return Component.extend({
            defaults: {
                template: 'Venerato_ProductAttributeCheckout/check-attribute'
            },

            //add here your logic to display step,
            isVisible: ko.observable(showStep()),
            stepCode: 'productAttributeCheckout',
            stepTitle: 'Product Attribute Checkout',

            /**
            *
            * @returns {*}
            */
            initialize: function () {
                this._super();
				
				console.log(((quote.getItems()[0]['product']['prop65'])!=null)&&(quote.getItems()[0]['product']['prop65']));
				if (((quote.getItems()[0]['product']['prop65'])!=null) && (quote.getItems()[0]['product']['prop65'])==1) {
					// register your step
					stepNavigator.registerStep(
						this.stepCode,
						//step alias
						null,
						this.stepTitle,
						//observable property with logic when display step or hide step
						this.isVisible,

						_.bind(this.navigate, this),

						/**
						* sort order value
						* 'sort order value' < 10: step displays before shipping step;
						* 10 < 'sort order value' < 20 : step displays between shipping and payment step
						* 'sort order value' > 20 : step displays after payment step
						*/
						15
					);
				}

                return this;
            },

            /**
            * The navigate() method is responsible for navigation between checkout step
            * during checkout. You can add custom logic, for example some conditions
            * for switching to your custom step
            */
            navigate: function () {

            },

            /**
            * @returns void
            */
            navigateToNextStep: function () {
                stepNavigator.next();
            }
        });
    }
);